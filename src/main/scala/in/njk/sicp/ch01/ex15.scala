/**
 * Author: '<a href="khalasi@gmail.com">Naresh Khalasi</a>'
 */

package in.njk.sicp.ch01

object ex15 extends App {
    // Exercise 1.5

    // (define (p) (p))
    def p:Int = p

    // (define (test x y)
    //      (if (= x 0) 0 y))
    def test(x:Int,  y:Int) = {
        println("Non terminating in case of applicative-order evaluation")
        if (x == 0) 0 else y
    }
    
    //println(test(0, p))  //Will not terminate since p is evaluated first and it results into an infinite recursion
    
    //Case II: Normal order evaluation
    def testTerminating(x:Int,  y: => Int) = {
        println("Terminates gracefully")
        if (x == 0) 0 else y
    }
    
    println(testTerminating(0, p))

  // Case III - passing a function allows evaluation when needed (normal-order) hence would terminate as well
  def myFunction() : Unit = myFunction
  def testTerminatingWithFunction(x : Int, y : () => Unit) = {
      println("Would terminate")
      if (x == 0) 0 else y
  }

  testTerminatingWithFunction(0, myFunction)
}

