package in.njk.sicp.ch01

/**
 * Author: '<a href="khalasi@gmail.com">Naresh Khalasi</a>'
 */


object ex110 extends App{

    def A(x:Int,  y:Int): Int = 
        if (y == 0) 0
        else if (x == 0) 2 * y
        else if (y == 1) 2
        else A((x - 1), A(x, (y - 1)))
    
    println(A(1,10))
    println(A(2,4))
    println(A(3,3))
    
    def f(n: Int) = A(0, n)
    def g(n: Int) = A(1, n)
    def h(n: Int) = A(2, n)
    def k(n: Int) = 5 * n * n
    
    println()
    println(f(5))
    println(g(10))
    println(h(4))
    println(k(2))
}
