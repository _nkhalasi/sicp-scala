/**
 * Author: '<a href="khalasi@gmail.com">Naresh Khalasi</a>'
 */

package in.njk.sicp.ch01

object ex16 extends App {
    // Exercise 1.6
    // Exercise 1.7 (see the isGoodEnough() implementation)

    def sqrt(x: Double) = {
      def abs(x: Double) = if (x < 0) -x else x

      def isGoodEnough(guess: Double) =
        abs(guess * guess - x) / x < 0.001

      def improve(guess: Double) =
        (guess + x / guess) / 2

      def sqrtIter(guess: Double): Double =
        if (isGoodEnough(guess)) guess
        else sqrtIter(improve(guess))

      sqrtIter(1.0)
    }
    
    println(sqrt(2))
    println(sqrt(4))
    println(sqrt(1e-6))
    println(sqrt(1e60))

    def newIf(predicateResult:Boolean,  thenClause:Double,  elseClause:Double): Double = 
        if (predicateResult) thenClause else elseClause
    
    
    def newSqrt(x: Double) = {
      def abs(x: Double) = if (x < 0) -x else x

      def isGoodEnough(guess: Double) =
        abs(guess * guess - x) / x < 0.001

      def improve(guess: Double) =
        (guess + x / guess) / 2

      def sqrtIter(guess: Double): Double =
        newIf(isGoodEnough(guess), guess, sqrtIter(improve(guess)))

      sqrtIter(1.0)
    }

//    println(newSqrt(2))

    //uncomment above to see the behavior

   /* You get a StackOverflowError in case of newSqrt()
    * This is because in case of regular "if" construct if the predicate evaluates to true,
    * the thenClause would be evaluated else elseClause would be evaluated.
    * In case of newIf, the elseClause is also evaluated using applicative order evaluation
    * which results into infinite recursion
    */
    
}


