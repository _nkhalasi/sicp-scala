/**
 * Author: '<a href="khalasi@gmail.com">Naresh Khalasi</a>'
 */

package in.njk.sicp.ch01

object ex13 extends App {

    def sum(a: Int, b: Int): Int = a + b

    def square(a: Int): Int = a * a

    def sumOf(f: (Int => Int))(a: Int, b: Int): Int = sum(f(a), f(b))

    val sumOfSquares = sumOf(square) _

    def sumOfSquaresOfLargestNumbers(a: Int, b: Int, c: Int): Int =
        if ((a < b) && (a < c)) sumOfSquares(b, c)
        else if ((b < a) && (b < c)) sumOfSquares(a, c)
        else sumOfSquares(a, b)

    println(sumOfSquaresOfLargestNumbers(1, 3, 4))
    println(sumOfSquaresOfLargestNumbers(3, 4, 1))
    println(sumOfSquaresOfLargestNumbers(4, 1, 3))
}