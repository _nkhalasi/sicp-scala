package in.njk.sicp.ch02

/**
 * Created by nkhalasi on 6/1/13 10:14 AM
 */

object Resistance {
    import Pair1._
    def makeInterval(x: Double, y: Double) = cons(x, y)
    def lowerBound(interval: (Int) => Double) = car(interval)
    def upperBound(interval: (Int) => Double) = cdr(interval)
    def addIntervals(interval1: (Int) => Double, interval2: (Int) => Double) =
        makeInterval(lowerBound(interval1) + lowerBound(interval2),
                     upperBound(interval1) + upperBound(interval2))
    def multiplyIntervals(interval1: (Int) => Double, interval2: (Int) => Double) = {
        val allPossibleBounds = lowerBound(interval1) * lowerBound(interval2) ::
                                lowerBound(interval1) * upperBound(interval2) ::
                                upperBound(interval1) * lowerBound(interval2) ::
                                upperBound(interval1) * upperBound(interval2) :: Nil
        makeInterval(allPossibleBounds.min, allPossibleBounds.max)
    }
    def divideIntervals(interval1: (Int) => Double, interval2: (Int) => Double) =
        multiplyIntervals(interval1,
                          makeInterval(1/upperBound(interval2),
                                       1/lowerBound(interval2)))
    def width(interval: (Int) => Double) =
        (upperBound(interval) - lowerBound(interval))/2
    def center(interval: (Int) => Double) =
        (lowerBound(interval) + upperBound(interval))/2
}