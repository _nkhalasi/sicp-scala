package in.njk.sicp.ch02

/**
 * Created by nkhalasi on 5/1/13 11:18 AM
 */
object Pair1 {
    def cons[T](x:T, y:T): (Int) => T = {
        def dispatch(idx: Int): T =
            if (idx == 0) x
            else if (idx == 1) y
            else throw new IllegalArgumentException("Index has to be 0 or 1")
        dispatch
    }

    def car[T](pair: (Int) => T) = pair(0)

    def cdr[T](pair: (Int) => T) = pair(1)
}


object Pair2 {
    def cons[T](x:T, y:T) = {
        def dispatch(a:T, b:T)(f: (T, T) => T) = f(a,b)
        dispatch(x, y)_
    }
    def car[T](pair: ((T, T) => T) => T) = {
        def dispatch(a:T, b:T) = a
        pair(dispatch)
    }
    def cdr[T](pair: ((T, T) => T) => T) = {
        def dispatch(a:T, b:T) = b
        pair(dispatch)
    }
}
