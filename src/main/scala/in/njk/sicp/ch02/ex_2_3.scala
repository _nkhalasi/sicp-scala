/**
 * Created by nkhalasi on 30/12/12 6:06 PM
 */

package in.njk.sicp.ch02

/**
 * Exercise 2.3: Implement a representation for rectangles in a plane.
 * (Hint: You may want to make use of Exercise 2.2.) In terms of
 * your constructors and selectors, create procedures that compute
 * the perimeter and the area of a given rectangle. Now implement
 * a different representation for rectangles. Can you design your sys-
 * tem with suitable abstraction barriers, so that the same perimeter
 * and area procedures will work using either representation?
 */
class Rectangle(private val top: Segment, private val bottom: Segment,
                private val left: Segment, private val right: Segment) {
    require(top.start.y == top.end.y, "Top should be a horizontal segment")
    require(bottom.start.y == bottom.end.y, "Bottom should be a horizontal segment")
    require(top.start.y > bottom.start.y, "Top should be above Bottom")
    require(left.start.x == left.end.x, "Left should be a vertical segment")
    require(right.start.x == right.end.x, "Right should be a vertical segment")
    require(right.start.x > left.start.x, "Left should be left of Right")
    require(top.start == left.end && top.end == right.end && bottom.end == right.start && bottom.start == left.start,
        "The segments should have common start and end points")
    val width = top.end.x - top.start.x
    val height = left.end.y - left.start.y

    def this(topLeft: Point, bottomRight: Point) = {
        this(new Segment(topLeft, new Point(bottomRight.x, topLeft.y)),
             new Segment(new Point(topLeft.x, bottomRight.y), bottomRight),
             new Segment(new Point(topLeft.x, bottomRight.y), topLeft),
             new Segment(bottomRight, new Point(bottomRight.x, topLeft.y)))
    }
    def perimeter = 2 * width + 2 * height
    def area = width * height
}

object Rectangle {
    def makeRectangle(topLeft: Point, bottomRight: Point) = new Rectangle(topLeft, bottomRight)
    def makeRectangle(top: Segment, bottom: Segment, left: Segment, right: Segment) = new Rectangle(top, bottom, left, right)
    def width(rectangle: Rectangle) = rectangle.width
    def height(rectangle: Rectangle) = rectangle.height
    def perimeter(rectangle: Rectangle) = 2 * width(rectangle) + 2 * height(rectangle)
    def area(rectangle: Rectangle) = width(rectangle) * height(rectangle)
}