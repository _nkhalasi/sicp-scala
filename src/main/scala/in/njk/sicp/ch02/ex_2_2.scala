/**
 * Created by nkhalasi on 30/12/2012 10:04 AM
 */
package in.njk.sicp.ch02

/**
 * Exercise 2.2: Consider the problem of representing line segments
 * in a plane. Each segment is represented as a pair of points:
 * a starting point and an ending point. Define a constructor
 * make-segment and selectors start-segment and end-segment
 * that define the representation of segments in terms of points.
 * Furthermore, a point can be represented as a pair of numbers:
 * the x coordinate and the y coordinate. Accordingly, specify a
 * constructor make-point and selectors x-point and y-point
 * that define this representation. Finally, using your selectors and
 * constructors, define a procedure midpoint-segment that takes
 * a line segment as argument and returns its midpoint (the point
 * whose coordinates are the average of the coordinates of the
 * endpoints). To try your procedures, you’ll need a way to print
 * points.
 */

case class Point(x:Double, y:Double) {
    override def toString() = "Point[x=" + x + ", y=" + y +"]"
}

case class Segment(start: Point, end: Point) {
    def midPoint = new Point((start.x + end.x)/2, (start.y + end.y)/2)
    override def toString() = "Segment[start:" + start + ", end:" + end + "]"
}


object Point {
    def makePoint(x: Double, y: Double) = new Point(x, y)
    def x(point: Point) = point x
    def y(point: Point) = point y
}

object Segment {
    def makeSegment(start: Point, end: Point) = new Segment(start, end)
    def start(segment: Segment) = segment start
    def end(segment: Segment) = segment end

    def midPoint(segment: Segment) =
        Point.makePoint((Point.x(segment.start) + Point.x(segment.end))/2,
                        (Point.y(segment.start) + Point.y(segment.end))/2)
}