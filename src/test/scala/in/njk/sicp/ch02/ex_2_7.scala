package in.njk.sicp.ch02

/**
 * Created by nkhalasi on 6/1/13 10:50 AM
 */

import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

import Resistance._

@RunWith(classOf[JUnitRunner])
class ResistanceSuite extends FunSuite {
    test("addition of two intervals") {
        val actual = addIntervals(makeInterval(1,2), makeInterval(2,3))
        val expected = makeInterval(3,5)
        assert(lowerBound(actual) === lowerBound(expected))
        assert(upperBound(actual) === upperBound(expected))
    }
    test("multiplication of two intervals") {
        val actual = multiplyIntervals(makeInterval(1,2), makeInterval(2,3))
        val expected = makeInterval(2,6)
        assert(lowerBound(actual) === lowerBound(expected))
        assert(upperBound(actual) === upperBound(expected))
    }
    test("division of two intervals") {
        val actual = divideIntervals(makeInterval(1,2), makeInterval(2,3))
        val expected = makeInterval(1.0/3.0, 1.0)
        assert(lowerBound(actual) === lowerBound(expected))
        assert(upperBound(actual) === upperBound(expected))
    }
}
