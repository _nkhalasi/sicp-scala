package in.njk.sicp.ch02
/**
 * Created by nkhalasi on 5/1/13 11:34 AM
 */

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Pair1Suite extends FunSuite {
    import Pair1._
    test("Construct pair of integers") {
        val pair = cons(10, 20)
        assert(10 === car(pair))
        assert(20 === cdr(pair))
    }
    test("Construct pair of names") {
        val pair = cons("Naresh", "Krishang")
        assert("Naresh" === car(pair))
        assert("Krishang" === cdr(pair))
    }
    test("Construct pair of mixed data") {
        val pair = cons("Naresh", 1)
        assert("Naresh" === car(pair))
        assert(1 === cdr(pair))
    }
}


@RunWith(classOf[JUnitRunner])
class Pair2Suite extends FunSuite {
    import Pair2._
    test("Construct pair of integers") {
        val pair = cons(10, 20)
        assert(10 === car(pair))
        assert(20 === cdr(pair))
    }
    test("Construct pair of names") {
        val pair = cons("Naresh", "Krishang")
        assert("Naresh" === car(pair))
        assert("Krishang" === cdr(pair))
    }
    test("Construct pair of mixed data") {
        val pair = cons("Naresh", 1)
        assert("Naresh" === car(pair))
        assert(1 === cdr(pair))
    }
}

