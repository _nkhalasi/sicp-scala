/**
 * Created by nkhalasi on 30/12/12 6:09 PM
 */

package in.njk.sicp.ch02

import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

@RunWith(classOf[JUnitRunner])
class RectangleSuite extends FunSuite {
    test("Construct Rectangle(topLeft, bottomRight)") {
        val rectangle = new Rectangle(new Point(2, 7), new Point(6, 2))
        assert(rectangle.width === 4)
        assert(rectangle.height === 5)
    }
    test("Construct Rectangle(topLeft, bottomRight) - using constructor and selector methods") {
        val rectangle = Rectangle.makeRectangle(new Point(2, 7), new Point(6, 2))
        assert(Rectangle.width(rectangle) === 4)
        assert(Rectangle.height(rectangle) === 5)
    }
    test("Construct Rectangle(top, bottom, left, right)") {
        val rectangle = new Rectangle(new Segment(new Point(2,7), new Point(6,7)),
                                      new Segment(new Point(2,2), new Point(6,2)),
                                      new Segment(new Point(2,2), new Point(2,7)),
                                      new Segment(new Point(6,2), new Point(6,7)))
        assert(rectangle.width === 4)
        assert(rectangle.height === 5)
    }
    test("Construct Rectangle(top, bottom, left, right) - using constructor and selector methods") {
        val rectangle = Rectangle.makeRectangle(Segment.makeSegment(Point.makePoint(2,7), Point.makePoint(6,7)),
                                      Segment.makeSegment(Point.makePoint(2,2), Point.makePoint(6,2)),
                                      Segment.makeSegment(Point.makePoint(2,2), Point.makePoint(2,7)),
                                      Segment.makeSegment(Point.makePoint(6,2), Point.makePoint(6,7)))
        assert(Rectangle.width(rectangle) === 4)
        assert(Rectangle.height(rectangle) === 5)
    }
    test("Area of Rectangle(topLeft, bottomRight)") {
        val rectangle = new Rectangle(new Point(2, 7), new Point(6, 2))
        assert(rectangle.area === 20)
    }
    test("Area of Rectangle(topLeft, bottomRight) - using constructor and selector methods") {
        val rectangle = Rectangle.makeRectangle(new Point(2, 7), new Point(6, 2))
        assert(Rectangle.area(rectangle) === 20)
    }
    test("Perimeter of Rectangle(topLeft, bottomRight)") {
        val rectangle = new Rectangle(new Point(2, 7), new Point(6, 2))
        assert(rectangle.perimeter === 18)
    }
    test("Perimeter of Rectangle(topLeft, bottomRight) - using constructor and selector methods") {
        val rectangle = Rectangle.makeRectangle(new Point(2, 7), new Point(6, 2))
        assert(Rectangle.perimeter(rectangle) === 18)
    }
    test("Rectangle Construction using non-aligned segments results into error") {
        intercept[IllegalArgumentException] {
            new Rectangle(new Segment(new Point(1, 2), new Point(4, 2)),
                          new Segment(new Point(1, 3), new Point(1, 3)),
                          new Segment(new Point(1, 2), new Point(1, 4)),
                          new Segment(new Point(10,2), new Point(10, 4)))
        }
    }
    test("Rectangle Construction using non-aligned segments results into error - using constructor method") {
        intercept[IllegalArgumentException] {
            Rectangle.makeRectangle(new Segment(new Point(1, 2), new Point(4, 2)),
                          new Segment(new Point(1, 3), new Point(1, 3)),
                          new Segment(new Point(1, 2), new Point(1, 4)),
                          new Segment(new Point(10,2), new Point(10, 4)))
        }
    }
}
