/**
 * Created by nkhalasi on 30/12/2012 10:24 AM
 */

package in.njk.sicp.ch02

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner


@RunWith(classOf[JUnitRunner])
class SegmentSuite extends FunSuite {
    test("Point object construction") {
        val point_repr = "Point[x=2.0, y=4.0]"
        val point = new Point(2,4)
        assert(point.toString === point_repr)
    }

    test("Point object construction using constructor method") {
        val point_repr = "Point[x=2.0, y=4.0]"
        val point = Point.makePoint(2,4)
        assert(point.toString === point_repr)
        assert(Point.x(point) === 2)
        assert(Point.y(point) === 4)
    }

    test("Segment object construction") {
        val segment_repr = "Segment[start:Point[x=2.0, y=4.0], end:Point[x=8.0, y=4.0]]"
        val start = new Point(2,4)
        val end = new Point(8,4)
        val segment = new Segment(start, end)
        assert(segment.toString === segment_repr)
        assert(Segment.start(segment) === start)
        assert(Segment.end(segment) === end)
    }

    test("Segment object construction using constructor method") {
        val segment_repr = "Segment[start:Point[x=2.0, y=4.0], end:Point[x=8.0, y=4.0]]"
        val segment = Segment.makeSegment(Point.makePoint(2,4), Point.makePoint(8,4))
        assert(segment.toString === segment_repr)
    }

    test("Mid point of a segment - different x, same y") {
        val segment = new Segment(new Point(2,4), new Point(8,4))
        val midpoint = new Point(5,4)
        assert(segment.midPoint === midpoint)
    }

    test("Mid point of a segment - different x, same y - using selector methods") {
        val segment = Segment.makeSegment(Point.makePoint(2,4), Point.makePoint(8,4))
        val midpoint = Point.makePoint(5,4)
        assert(Segment.midPoint(segment) === midpoint)
    }

    test("Mid point of a segment - same x, different y") {
        val segment = new Segment(new Point(4,2), new Point(4,8))
        val midpoint = new Point(4,5)
        assert(segment.midPoint === midpoint)
    }

    test("Mid point of a segment - same x, different y - using selector methods") {
        val segment = Segment.makeSegment(Point.makePoint(4,2), Point.makePoint(4,8))
        val midpoint = Point.makePoint(4,5)
        assert(Segment.midPoint(segment) === midpoint)
    }

    test("Mid point of a segment - different x, different y") {
        val segment = new Segment(new Point(2,2), new Point(4,8))
        val midpoint = new Point(3,5)
        assert(segment.midPoint === midpoint)
    }

    test("Mid point of a segment - different x, different y - using selector methods") {
        val segment = Segment.makeSegment(Point.makePoint(2,2), Point.makePoint(4,8))
        val midpoint = Point.makePoint(3,5)
        assert(Segment.midPoint(segment) === midpoint)
    }

    test("Mid point of a segment - coordinates are not absolute integers") {
        val segment = new Segment(new Point(2.25,2.45), new Point(4.54,8.50))
        val midpoint = new Point(3.395,5.475)
        assert(segment.midPoint === midpoint)
    }

    test("Mid point of a segment - coordinates are not absolute integers - using selector methods") {
        val segment = Segment.makeSegment(Point.makePoint(2.25,2.45), Point.makePoint(4.54,8.50))
        val midpoint = Point.makePoint(3.395,5.475)
        assert(Segment.midPoint(segment) === midpoint)
    }
}
